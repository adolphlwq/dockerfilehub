syntax on
filetype on
colorscheme desert

autocmd FileType c,cpp : set foldmethod=syntax
autocmd FileType c,cpp :set number
autocmd FileType c,cpp :set cindent

set number
set tabstop=4
set shiftwidth=4

set expandtab
set incsearch
set hlsearch

set autoindent
set smartindent

set showmatch
set ruler
