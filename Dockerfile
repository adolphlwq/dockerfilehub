#base image:change sources.list from USTC,update and inatall vim
#
#version:0.0.1

FROM ubuntu
MAINTAINER adolphlwq <kenan3015@gmail.com>

LABEL Description="it is used as a basic image for DuoHuoStudio and my study.I will update and install vim." Vendor="Basic image"

#backup sources.list
RUN cd /etc/apt/ && mv sources.list sources.list_backup

#add source.list
ADD sources.list /etc/apt/ 

#refresh time
ENV REFRESHED_AT 2015-05-18
#update and install vim
RUN apt-get update && apt-get upgrade -y && apt-get install -y vim
RUN cd /etc/vim/ && mv vimrc vimrc.backup

ADD vimrc /etc/vim/

CMD ["/bin/bash"]
